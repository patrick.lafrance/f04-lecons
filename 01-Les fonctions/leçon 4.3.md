Certaines fonctions facilitent le travail avec les dates et heures : 

CURDATE() ou CURRENT_DATE() : Retourne la date courante

CURTIME() ou CURRENT_TIME() : Retourne l'heure courante

DATE(datetime) : Retourne la partie date d'un DATETIME

TIME(datetime) : Retourne la partie heure d'un DATETIME

DAY(date), MONTH(date), YEAR(date) : retournent le jour, mois ou année d'une date ou datetime

HOUR(time), MINUTE(time) : retournent l'heure ou les minutes d'un time ou datetime

etc.

Voir la <a href="https://mariadb.com/kb/en/date-time-functions/" target="_blank">liste complète</a>.

### Exemples :

Vous désirez connaître tous les employés qui partagent votre date d'anniversaire (disons, le 1er avril)?

    SELECT nom, prénom FROM employé WHERE DAY(date_de_naissance)=1 AND 
	                                      MONTH(date_de_naissance)=4;
	
Mieux, vous voulez savoir quels employés fêtent leur anniversaire aujourd'hui (pour leur envoyer une carte et un petit gâteau) : 

    SELECT nom, prénom FROM employé WHERE DAY(date_de_naissance)=DAY(CURRENT_DDATE()) AND 
	                                      MONTH(date_de_naissance)=MONTH(CURRENT_DDATE());

Vous voulez supprimer tous les employés nés au mois d'octobre (par pure méchanceté) :

    DELETE FROM employé WHERE MONTH(date_de_naissance)=10;
	
	
