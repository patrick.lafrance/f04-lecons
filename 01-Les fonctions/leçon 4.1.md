Une des différences entre les fonctions mathématiques et les fonction en programmation, sont les types de données sur lesquels elles travaillent. En programmation, les données avec lesquelles on travaille peuvent être des nombres, entiers ou réels mais aussi des chaînes de caractères, des booléens, des dates, etc. Les fonctions peuvent recevoir et retourner n'importe lesquels de ces types.

On peut classer les fonctions selon le type de données avec lesquelles elles travaillent.

## fonctions numériques

Nous avons déjà vu une fonction numérique, LEAST. Il en existe plusieurs autres :

GREATEST(x, y) : retourne le plus grand nombre entre x et y

ROUND(x) : retourne x, arrondi à l'entier le plus près

POWER(x, y) : retourne x élévé à la puissance y (x<sup>y</sup>)

ABS(x) : retourne la valeur absolue de x.

etc.

Voir la <a href="https://mariadb.com/kb/en/numeric-functions/" target="_blank">liste complète</a>.

### Exemples :

Obtenir le boni calculé dans la leçon précédente, arrondi au cent près :

    SELECT no_compte, ROUND( LEAST(solde*0.01, 2000) ) AS boni FROM compte WHERE solde>0;

    +-----------+------------+
    | no_compte | boni       |
    +-----------+------------+
    |  10906680 |         70 |
    |  10189439 |         23 |
    |  06439223 |        215 |
    |  10898966 |        335 |
    |  03309088 |        253 |
    |  04768315 |       2000 |
    |  08843017 |       2000 |
    |  06102499 |       2000 |
    +-----------+------------+
