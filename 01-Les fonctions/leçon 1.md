Vous avez sans doute déjà entendu le terme «fonction», soit en mathématique, soit en programmation. Dans les deux domaines, le terme est très utilisé et décrit sensiblement la même chose. Ce n'est pas très surprenant, puisque les premiers informaticiens étaient très souvent des mathématiciens et ils ont naturellement eu recours à leur jargon mathématique pour nommer les nouveaux concepts informatiques.

En mathématique, une fonction est une correspondance de chaque élément d'un ensemble avec un autre élément du même ensemble ou d'un autre.

Par exemple, voici la fonction f qui fait correspondre chaque élément du groupe de gauche avec un élément du groupe de droite :

    🌞 → 😎
    🍎 → 👅
    🎂 → 👅
    ☁  → ☂
	     💩

Remarquez que cette correspondance est à sens unique. À chaque élément du «domaine» (le groupe de gauche) est associé un et un seul élément de l'«image» (le groupe de droite). L'inverse n'est pas vrai; certains élément de l'image ne sont pas associés (ici, le tas de... chocolat?) ou le sont plus d'une fois (ici, la langue).

Bien entendu, en mathématique, les fonctions servent plutôt à associer des nombres que des images. Par exemple : 

	-2 → 4
	-1 → 1
	 0 → 0
	 1 → 1
	 2 → 4
	 3 → 9
	
Remarquez, encore une fois qu'à chaque nombre du domaine correspond un et un seul nombre de l'image mais pas l'inverse.

Généralement, le domaine contient bien plus que quelques éléments comme dans ces exemples (typiquement, le domaine est l'ensemble des nombres entiers ou des nombres réels, lesquels comportent une infinité d'éléments). Pour abréger, on notera une fonction par un nom et une description mathématique de la relation. Par exemple, la fonction précédente peut être notée de la façon suivante :

    f(x) = x²

ou en mots : la fonction f associe un nombre appelé x au résultat de l'évaluation de l'expression x².

f est le nom (très peu original) de la fonction, x est appelé un paramètre et x² est l'expression à évaluer pour obtenir l'image correspondant à un paramètre donné.

En remplaçant x par quelques nombres, on obtient : 

	f(-2) = -2²
	f(-1) = -1²
	f( 0) =  0²
	f( 1) =  1²
	f( 2) =  2²
	f( 3) =  3²

ou, après avoir évalué l'expression : 

	f(-2) = 4
	f(-1) = 1
	f( 0) = 0
	f( 1) = 1
	f( 2) = 4
	f( 3) = 9

ce qui ressemble bien à notre exemple.
