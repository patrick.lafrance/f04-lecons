Qu'en est-il du SQL? Est-ce qu'on peut utiliser les fonctions de la même façon qu'avec d'autres langages comme Python ou C++?

Oui.

SQL n'est pas qu'un outil d'interrogation de bases de données; il s'agit d'un langage de programmation complet permettant de programmer n'importe quel calcul (on dit alors qu'il est <a href="https://fr.wikipedia.org/wiki/Turing-complet#Langages_de_programmation_Turing-complets" target="_blank">Turing-complet</a> en référence à <a href="https://fr.wikipedia.org/wiki/Alan_Turing" target="_blank">Alan Turing</a>). On peut même, apparemment, programmer des jeux en SQL!

SQL possède la notion de fonction, tout comme Python ou C++. Dans le cadre de ce cours, nous n'aurons ni le temps ni le besoin de définir nos propres fonctions, mais nous allons pouvoir utiliser celles qui sont déjà présentes dans le SGBD MariaDB (et qui sont suffisantes pour tout ce qu'on aura besoin de faire).

En SQL, les fonctions s'utilisent de la même manière qu'en Python, en les appelant par leur nom et en leur passant des paramètres entre parenthèses. Comme pour presque tout en SQL, on écrit généralement les noms de fonction en majuscule, bien que ça ne soit pas requis par le SGBD.

On peut utiliser les fonctions dans n'importe quelle expression faisant partie d'une requête LMD (SELECT, UPDATE, INSERT ou DELETE).

Rappelez-vous la syntaxe (simplifiée) de ces requêtes :

SELECT : 

    SELECT expressions FROM table [WHERE condition];
	
	expressions::=expression [, expression ...]
    condition::= expression_booléenne [(AND|OR) expression_booléenne ...]
	
INSERT :

    INSERT INTO table [ (champs) ] VALUES enregistrement [, enregistrement ... ];
	
	champs::= champ [, champ ...]
	enregistrement::= (expression [, expression ... ])

UPDATE : 

    UPDATE table SET modification [, modification ...];
	
	modification::= champ = expression

DELETE :

	DELETE FROM table [WHERE condition];
	
    condition::= expression_booléenne [(AND|OR) expression_booléenne ...]


Partout où se trouve une expression, vous pouvez utiliser des fonctions.

Exemples : 

Vous travaillez pour la banque de Rosemont. Cette banque généreuse (c'est un <a href="https://fr.wikipedia.org/wiki/Oxymore" target="_blank">oxymore</a>) offre à ses clients un cadeau de Noël sous la forme d'un boni de 1% du solde de leur compte d'épargne... jusqu'à concurrence de 20$ par compte. En tant que développeur, on vous demande de créer la requête SQL permettant de faire ce calcul pour chacun des comptes actifs.

Vous essayez quelque chose :

    SELECT no_compte, solde*0.01 AS boni FROM compte WHERE solde>0;
	
et vous obtenez effectivement un résultat intéressant dont voici un extrait : 

    +-----------+------------+
    | no_compte | boni       |
    +-----------+------------+
    |  10906680 |    70.2800 |
    |  10189439 |    23.0600 |
    |  06439223 |   214.6600 |
    |  10898966 |   335.0500 |
    |  03309088 |   252.6700 |
    |  05781966 | 19536.1800 |
    |  01075574 | 22898.9400 |
    |  06102499 |  9234.2600 |
    +-----------+------------+

Considérant que le solde (et donc le boni) est exprimé en cents, il semble que ça ne soit pas encore parfait. Votre employeur avait demandé de limiter le boni à 20$ par compte avec un solde positif. Avec cette requête, le compte 01075574 recevrait 228,99$... comment limiter le boni à 20$?

Entre en scène la fonction LEAST qui retourne le plus petit de deux nombres. Ainsi, on peut calculer le boni comme étant la plus petite valeur entre 1% du solde et 20$ (soit 2000¢).

    SELECT no_compte, LEAST(solde*0.01, 2000) AS boni FROM compte WHERE solde>0;
	
et vous obtenez effectivement un résultat intéressant dont voici un extrait : 

    +-----------+------------+
    | no_compte | boni       |
    +-----------+------------+
    |  10906680 |    70.2800 |
    |  10189439 |    23.0600 |
    |  06439223 |   214.6600 |
    |  10898966 |   335.0500 |
    |  03309088 |   252.6700 |
    |  05781966 |  2000.0000 | <- limité à 20$
    |  01075574 |  2000.0000 |
    |  06102499 |  2000.0000 |
    +-----------+------------+

On peut même imaginer octroyer le boni automatiquement en utilisant une requête UPDATE : 

    UPDATE compte SET solde = solde + LEAST(solde*0.01, 2000) WHERE solde>0;

Après l'exécution, on obtient :

    SELECT no_compte, solde FROM compte;

    +-----------+---------+
    | no_compte | solde   |
    +-----------+---------+
    |  10906680 |    7098 | <- 70,28$ + boni de 0,70$
    |  10189439 |    2329 |
    |  06439223 |   21681 |
    |  10898966 |   33840 |
    |  03309088 |   25520 |
    |  05781966 | 1955618 | <- 19536,18$ + boni de 20$
    |  01075574 | 2291894 |
    |  09398203 | 1928326 |
    +-----------+---------+

