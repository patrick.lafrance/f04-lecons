Quant est-il des fonctions en programmation?

À peu de choses près, il s'agit du même concept. Une fonction fait correspondre (on dira qu'elle retourne) une valeur à un ou plusieurs paramètres lui étant fournis. La règle qui en mathématique impose qu'une fonction fasse correspondre une et une seule valeur à tous les éléments du domaine ne s'applique pas aussi fortement en programmation mais por le moment, nous pouvons considérer que c'est aussi le cas.

Prenons l'exemple précédent et transformons-le en fonction informatique. La fonction f fait correspondre le carré d'un nombre à ce nombre nommé x :

    f(x) = x²

En programmation, on dira que la fonction *carré* retourne le carré d'un paramètre x. En Python, ça donnerait ceci :

    def carré(x) : 
	    return x**2
		
Outre les noms généralement plus longs (*f* vs *carré*, les mathématiciens sont sans doute plus paresseux que les programmeurs) et les symboles qui changent (*return* plutôt que = et **2 plutôt que ²), les seules différences sont dans la façon d'écrire la fonction pour répondre aux caprices de compilateur.

Tout comme en mathématique, les fonctions en programmation peuvent recevoir plusieurs paramètres. La fonction en Python suivante :

    def addition(x, y) :
	    return x + y
		
correspondrait à la fonction mathématique : 

    a(x,y) = x+y
	
qui donne la somme d'une paire de nombres x et y. En lui fournissant n'importe qu'elle paire de nombres, on obtient un et un seul résultat : 

    a(6, 2) = 6+2 = 8
    a(-1, 4) = -1+4 = 3
    a(6234, -15762) = 6+2 = 9528
	
ou en Python :

    addition(6, 2) retourne 8
    addition(-1, 4) retourne 3
    addition(6234, -15762) retourne 9528

Et qu'est-ce qu'on peut faire avec ces valeurs *retournées* par une fonction? Tout simplement, les utiliser dans une expression :

	somme = addition(6, 2) # la somme de 6 et 2
	somme = 2 + addition(-1, 4) # 2 + la somme de -1 et 4
	somme = 2 + addition(5, 1) + addition(8, 3) # 2 + la somme de 5 et 1 + la somme de 8 et 3
	somme = 2 + addition(5, addition(8, 21/3) + 22 ) # ???
	
Ça commence à être compliqué? Pas de panique! Suivez simplement la priorité des opérations. Vous avez remarqué les parenthèses qui suivent nécessairement le nom de la fonction? Considérez-les de la même façon que les parenthèses en mathématique et effectuez les opération qui se trouvent à l'intérieur d'abord. Reprenons le dernier exemple :

  	somme = 2 + addition(5, addition(8, 21/3) + 22 )
	
Allons-y étape par étape. D'abord, l'intérieur des parenthèses intérieures :

  	somme = 2 + addition(5, addition(8, 21/3) + 22 )
	                                      ↓
  	somme = 2 + addition(5, addition(8,   7 ) + 22)
	
Lorsqu'il n'y a plus d'expressions non évaluées	dans la parenthèse, évaluez la fonction elle-même. On sait que la fonction *addition* retourne la somme des deux paramètres qui lui sont passés : 

  	somme = 2 + addition(5, addition(8, 7) + 22 )
	                                ↓
  	somme = 2 + addition(5,         15     + 22)

Recommençons avec l'intérieur des parenthèses restantes :

  	somme = 2 + addition(5, 15 + 22)
	                           ↓ 
  	somme = 2 + addition(5,    37  )
       	                ↓ 
  	somme = 2 +         42
              ↓ 
  	somme =   44
	
