Il existe aussi beaucoup de fonctions s'appliquant sur des chaînes de caractères :

CONCAT(chaîne1, chaîne2) : retourne les chaînes concaténées (fusionnées).

INSTR(botte\_de\_foin, aiguille) : retourne l'indice de la position de *aiguille* dans *botte_de_foin*.

LEFT(chaîne, n), RIGHT(chaîne, n) : retourne les n premiers caractères de *chaîne*, à partir respectivement de la gauche et de la droite.

LOWER(chaîne) : retourne *chaîne* entièrement en minuscules

UPPER(chaîne) : retourne *chaîne* entièrement en majuscules

REPLACE(chaîne, recherché, remplacement) : retourne la *chaîne* dont chaque occurence de *recherché* a été remplacé par *remplacement* 

TRIM(chaîne) : retourne *chaîne* moins tous les espaces qui pourraient se trouver au début ou à la fin

etc.

Voir la <a href="https://mariadb.com/kb/en/string-functions/" target="_blank">liste complète</a>.

### Exemples :

Imaginez qu'on vous demande de générer les noms d'utilisateur de tous les employés d'une compagnie, formés par la première lettre de leur prénom suivie de leur nom de famille complet.

Obtenir les noms et prénoms en minuscules d'employés.

    SELECT LOWER(prénom) AS prénom, LOWER(nom) AS nom FROM employés;
    +------------+----------+
    | prénom     | nom      |
    +------------+----------+
    | madelgarde | thibault |
    | xaviel     | magnan   |
    | cérélise   | ranger   |
    | georgéus   | guy      |
    | médérée    | prince   |
    +------------+----------+

Obtenir l'initiale du prénom et le nom de famille en minuscules d'employés :

    SELECT LOWER(LEFT(prénom,1)) AS initiale, LOWER(nom) AS nom FROM employés;
    +----------+----------+
    | initiale | nom      |
    +----------+----------+
    | m        | thibault |
    | x        | magnan   |
    | c        | ranger   |
    | g        | guy      |
    | m        | prince   |
    +----------+----------+	

Concatener l'initiale du prénom et le nom de famille en minuscules d'employés :

    SELECT CONCAT(LOWER(LEFT(prénom, 1)), LOWER(nom)) AS nom_utilisateur FROM employés;
    +-----------------+
    | nom_utilisateur |
    +-----------------+
    | mthibault       |
    | xmagnan         |
    | cranger         |
    | gguy            |
    | mprince         |
    +-----------------+

(remarquez à quel point les alias (AS ...) deviennent utiles ici!)

