Nous venons de voir qu'à deux entités distinctes doivent correspondrent deux tables distinctes.

Nous adapterons notre base de données de la SAAQ avec les tables *propriétaire* :

    DESC propriétaire;
    +-----------+--------------+------+-----+---------+-------+
    | Field     | Type         | Null | Key | Default | Extra |
    +-----------+--------------+------+-----+---------+-------+
    | id        | int(11)      | NO   | PRI | NULL    |       | <- clé PRImaire
    | no_permis | varchar(255) | YES  |     | NULL    |       |
    | nom       | varchar(255) | NO   |     | NULL    |       |
    | prénom    | varchar(255) | NO   |     | NULL    |       |
    | adresse   | varchar(255) | YES  |     | NULL    |       |
    +-----------+--------------+------+-----+---------+-------+
	                                    ^
								PRI pour «primaire»

 et *véhicule* :
 
    DESC véhicule;
    +-----------------+--------------+------+-----+---------+-------+
    | Field           | Type         | Null | Key | Default | Extra |
    +-----------------+--------------+------+-----+---------+-------+
    | no_série        | varchar(255) | NO   |     | NULL    |       |
    | immatriculation | varchar(7)   | NO   |     | NULL    |       |
    | marque          | varchar(255) | NO   |     | NULL    |       |
    | modèle          | varchar(255) | NO   |     | NULL    |       |
    | année           | int(11)      | NO   |     | NULL    |       |
    +-----------------+--------------+------+-----+---------+-------+

Encore une fois, il faudra choisir une clé primaire.
