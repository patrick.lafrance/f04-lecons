Une base de données  est dite *relationnelle* lorsqu'elle met en relation les diverses entités qu'elle représente par des tables.
Soit le cas de figure suivant :

La Société d'Assurance Automobile du Québec possède une BD dans laquelle sont enregistrés les données sur le propriétaires et leurs véhicules. La table propriétaire pourrait ressembler au minimum à ceci (dans la réalité, elle risque d'être immensément plus complexe) :

    DESC propriétaire;
    +-----------+--------------+------+-----+---------+-------+
    | Field     | Type         | Null | Key | Default | Extra |
    +-----------+--------------+------+-----+---------+-------+
    | no_permis | varchar(255) | YES  |     | NULL    |       |
    | nom       | varchar(255) | NO   |     | NULL    |       |
    | prénom    | varchar(255) | NO   |     | NULL    |       |
    | adresse   | varchar(255) | YES  |     | NULL    |       |
    +-----------+--------------+------+-----+---------+-------+

Cette table permettrait de stoquer les informations minimales concernant les propriétaires de véhicule.

Par exemple : 

    SELECT * FROM propriétaire;
    +---------------+---------+---------+------------------------------------+
    | no_permis     | nom     | prénom  | adresse                            |
    +---------------+---------+---------+------------------------------------+
    | s213502031978 | Simpson | Homer   | 742 Evergreen Terrace, Springfield |
    | g349821031957 | Gratton | Robert  | 2121 des Jonquilles, Longueuil     |
    +---------------+---------+---------+------------------------------------+

Il manque quelque chose de très important à cette table : une clé primaire. TOUTES les tables doivent avoir une clé primaire. 

Quel champ choisiriez-vous pour agir comme clé primaire pour la table propriétaire ?
