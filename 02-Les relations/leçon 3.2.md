Nous avons vu la cardinalité des relations entre les tables. Remarquez qu'il s'agit bien d'une propriété des relations et non des tables. C'est-à-dire qu'on parle toujours de la cardinalité d'une relation et non de la cardinalité d'une table. À preuve, la même table peut être impliquée dans plus d'une relation avec des cardinalités complètement différentes :

![saaq 4](https://www.dropbox.com/s/agzqtpad8ytci7a/saaq_4.jpeg?dl=1)

La relation entre Véhicule et Propriétaire est de cardinalité *un à plusieurs* alors que celle entre Propriétaire et Dossier de conduite est de cardinalité *un à un* (un propriétaire possède un dossier de conduite et un dossier de conduite ne concerne qu'un propriétaire).

Pourtant, à bien y réfléchir, une information utile manque à propos de ces relations. On sait qu'un propriétaire peut avoir plusieurs véhicules et au maximum un dossier de conduite. Ce sont des cardinalités maximales. Mais qu'en est-il du minimum requis? Est-ce qu'un propriétaire DOIT posséder au moins un véhicule? Est-ce qu'il DOIT posséder au moins un dossier?

Il s'agit maintenant de connaître la cardinalité minimale d'une relation, c'est-à-dire combien d'éléments de chaque entité doivent participer à la relation au minimum, un ou zéro. Si la relation est obligatoire (le véhicule DOIT avoir un propriétaire), la cardinalité minimale est de 1. Si la relation est facultative (le propriétaire PEUT avoir dossier de conduite mais ce n'est pas obligatoire), la cardinalité est de 0.

Reprenons notre exemple pour y ajouter une cardinalité minimale :

![saaq 3](https://www.dropbox.com/s/0s18u5ex7dvw0ix/saaq_3.jpeg?dl=1)

On doit ajouter un minimum de chaque côté de la relation. Commençons par le côté du Propriétaire. Dans une relation propriétaire-véhicule, est-ce qu'un véhicule DOIT avoir un propriétaire? Au moins au sens de la SAAQ, j'imagine mal un véhicule sans propriétaire (même la carcasse de voiture laissée sur le bord d'une voie ferrée appartient «officiellement» à quelqu'un). On peut donc dire que le propriétaire est obligatoire dans la relation, qu'un et un seul propriétaire possède un véhicule. Nous allons représenter cette cardinalité minimale par un autre 1 du côté intérieur de la relation : 

![saaq 5](https://www.dropbox.com/s/cffxjp7fcyu4cwh/saaq_5.jpeg?dl=1)

et on lit cette relation «(au moins) un et un seul propriétaire» possède un véhicule.

De l'autre côté, est-ce qu'un propriétaire DOIT posséder un véhicule? C'est-à-dire, existe-t-il des propriétaires qui ne possèdent aucun véhicule? Il s'agit d'une question à laquelle le client et les utilisateurs du système pourraient facilement répondre mais avec un petit effort d'imagination on peut y répondre nous-même. Imaginez un propriétaire de véhicule qui vend sa voiture. Que doit-il arriver à ses informations dans notre BD? Doit-on les supprimer? S'il possède un solde non payé à son compte, ce n'est peut-être pas une bonne idée. De plus, s'il achète un nouveau véhicule plus tard, il serait pratique d'avoir déjà toutes les informations sur lui (dont son historique de propriété et de paiement). On peut donc dire qu'un propriétaire peut exister dans la base de donnée sans posséder un véhicule. Ou, autrement dit, qu'un propriétaire possède 0, 1 ou plusieurs véhicules. 

Nous allons réprésenter cette cardinalité minimale du côté du véhicule. En effet, c'est l'identifiant du propriétaire qui doit être exporté dans 0, 1 ou plusieurs entités de Véhicule :

![saaq 6](https://www.dropbox.com/s/m0kqgcl86ya4ev7/saaq_6.jpeg?dl=1)

Imaginez les cardinalités comme le nombre de branches que devrait avoir la patte d'oie dans la réalité. Si pour chaque véhicule que possède un propriétaire on dessine une patte du côté du véhicule (parce qu'on y exporte son id), on peut dessiner entre 0 et plusieurs pattes.

Pour aider la lecture des cardinalités, on ajoute souvent un verbe au-dessus de la relation qui indique de quelle façon sont reliées les entites : 

![saaq 7](https://www.dropbox.com/s/xpjbm8uc2vun0mi/saaq_7.jpeg?dl=1)

se lit «un et un seul propriétaire POSSÈDE 0, 1 ou plusieurs véhicules».

Si on complète le schéma :

![saaq 8](https://www.dropbox.com/s/scszkkr3ie5ycds/saaq_8.jpeg?dl=1)

On peut lire la relation entre Propriétaire et Dossier de conduite : «un en un seul propriétaire possède zéro ou un dossier de conduite».
