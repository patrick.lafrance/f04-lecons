Nous allons prendre la clé primaire identifiant le propriétaire et nous allons en faire une copie dans la table véhicule:

    SELECT * FROM propriétaire;
    +----+---------------+---------+---------+------------------------------------+
    | id | no_permis     | nom     | prénom  | adresse                            |
    +----+---------------+---------+---------+------------------------------------+
    |  1 | s213502031978 | Simpson | Homer   | 742 Evergreen Terrace, Springfield |
    |  2 | g349821031957 | Gratton | Robert  | 2121 des Jonquilles, Longueuil     |
    +----+---------------+---------+---------+------------------------------------+
   	   |
	   .---------------------------------------------------------------------------------.
																						 |
    SELECT * FROM véhicule;															     🠋
    +-------------+-----------------+--------------+-------------+--------+--------------------------------+
    | no_série    | immatriculation | marque       | modèle      | année  | id du propriétaire du véhicule |
    +-------------+-----------------+--------------+-------------+--------+--------------------------------+
    | C8952457553 | F42680          | Chevrolet    | Camaro      |   1968 |              2                 |
    | H00000001   | 123 ABC         | Powell Motor | Homermobile |   1991 |              1                 |
    | GKR7498779  | GKY 664         | Ford         | Pinto       |   1971 |              2                 |
    +-------------+-----------------+--------------+-------------+--------+--------------------------------+

Pour ce faire, nous devons au préalable avoir prévu une colonne dans la table véhicule pour recevoir cette valeur. Cette colonne se nomme une *clé étrangère*. On dit qu'elle est étrangère parce que les valeurs qu'elle contient proviennent toutes d'une autre table (souvent, il s'agit d'une copie de la clé primaire, mais pas toujours).

On dira aussi qu'on *importe* le champ *id* de la table propriétaire dans la table *véhicule*.

    DESC propriétaire;
    +-----------+--------------+------+-----+---------+-------+
    | Field     | Type         | Null | Key | Default | Extra |
    +-----------+--------------+------+-----+---------+-------+
    | id        | int(11)      | NO   | PRI | NULL    |       | >-clé primaire-------.
    | no_permis | varchar(255) | YES  |     | NULL    |       |                      |
    | nom       | varchar(255) | NO   |     | NULL    |       |                      |
    | prénom    | varchar(255) | NO   |     | NULL    |       |                      |
    | adresse   | varchar(255) | YES  |     | NULL    |       |                      |
    +-----------+--------------+------+-----+---------+-------+                      |
                                                                                     |
    DESC véhicule;                                                                   |
    +-----------------+--------------+------+-----+---------+-------+                |
    | Field           | Type         | Null | Key | Default | Extra |                |
    +-----------------+--------------+------+-----+---------+-------+                |
    | no_série        | varchar(255) | NO   | PRI | NULL    |       |                |
    | immatriculation | varchar(7)   | NO   |     | NULL    |       |                |
    | marque          | varchar(255) | NO   |     | NULL    |       |                |
    | modèle          | varchar(255) | NO   |     | NULL    |       |                |
    | année           | int(11)      | NO   |     | NULL    |       |                |
    | propriétaire_id | int(11)      | NO   |     | NULL    |       |<-clé étrangère-.
    +-----------------+--------------+------+-----+---------+-------+

Remarquez que puisque la clé étrangère est une copie de la clé primaire, elle doit être du même type (ici, INT). Il ne serait pas logique de vouloir copier le contenu d'une VARCHAR dans un INT ou même d'un CHAR(8) dans un CHAR(6).

Par convention et pour repérer facilement la provenance d'une clé étrangère, celle-ci prendra le nom de la combinaison table\_champ d'où elle provient. Par exemple ici, puisque la clé étrangère est une copie du champ id de la table propriétaire, nous l'avons nommée propriétaire\_id.

Une fois implémentée correctement la relation entre les deux tables, nous aurons le résultat suivant :

    SELECT * FROM véhicule;
    +-------------+-----------------+--------------+-------------+--------+-----------------+
    | no_série    | immatriculation | marque       | modèle      | année  | propriétaire_id |
    +-------------+-----------------+--------------+-------------+--------+-----------------+
    | C8952457553 | F42680          | Chevrolet    | Camaro      |   1968 |               2 |
    | H00000001   | 123 ABC         | Powell Motor | Homermobile |   1991 |               1 |
    | GKR7498779  | GKY 664         | Ford         | Pinto       |   1971 |               2 |
    +-------------+-----------------+--------------+-------------+--------+-----------------+

