Voici donc le schéma de la table propriétaire avec sa clé primaire artificielle (id) :

    DESC propriétaire;
    +-----------+--------------+------+-----+---------+-------+
    | Field     | Type         | Null | Key | Default | Extra |
    +-----------+--------------+------+-----+---------+-------+
    | id        | int(11)      | NO   | PRI | NULL    |       |
    | no_permis | varchar(255) | YES  |     | NULL    |       |
    | nom       | varchar(255) | NO   |     | NULL    |       |
    | prénom    | varchar(255) | NO   |     | NULL    |       |
    | adresse   | varchar(255) | YES  |     | NULL    |       |
    +-----------+--------------+------+-----+---------+-------+

Cette table permettrait de stoquer les informations minimales concernant les propriétaires de véhicule.

Par exemple : 

    SELECT * FROM propriétaire;
    +----+---------------+---------+---------+------------------------------------+
    | id | no_permis     | nom     | prénom  | adresse                            |
    +----+---------------+---------+---------+------------------------------------+
    |  1 | s213502031978 | Simpson | Homer   | 742 Evergreen Terrace, Springfield |
    |  2 | g349821031957 | Gratton | Robert  | 2121 des Jonquilles, Longueuil     |
    +----+---------------+---------+---------+------------------------------------+

Si l'on veut maintenant ajouter les informations sur les véhicules que possède ces personnes, on pourrait être tenté d'ajouter des champs à la table propriétaire : 

    DESC propriétaire;
    +------------------+--------------+------+-----+---------+-------+
    | Field            | Type         | Null | Key | Default | Extra |
    +------------------+--------------+------+-----+---------+-------+
    | id               | int(11)      | NO   | PRI | NULL    |       | ⎫
    | no_permis        | varchar(255) | YES  |     | NULL    |       | ⎪
    | nom              | varchar(255) | NO   |     | NULL    |       | ⎬proprio
    | prénom           | varchar(255) | NO   |     | NULL    |       | ⎪
    | adresse          | varchar(255) | YES  |     | NULL    |       | ⎭
    | no_série         | varchar(255) | YES  |     | NULL    |       | ⎫
    | immatriculation  | varchar(7)   | YES  |     | NULL    |       | ⎪
    | marque           | varchar(255) | YES  |     | NULL    |       | ⎬véhicule
    | modèle           | varchar(255) | YES  |     | NULL    |       | ⎪
    | année            | int(11)      | YES  |     | NULL    |       | ⎭
    +------------------+--------------+------+-----+---------+-------+

Les données dans la table ressembleraient alors à ceci :

    +----+---------------+---------+---------+------------------------------------+-------------+-----------------+--------------+-------------+--------+
    | id | no_permis     | nom     | prénom  | adresse                            | no_série    | immatriculation | marque       | modèle      | année  |
    +----+---------------+---------+---------+------------------------------------+-------------+-----------------+--------------+-------------+--------+
    |  1 | s213502031978 | Simpson | Homer   | 742 Evergreen Terrace, Springfield | H00000001   | 123 ABC         | Powell Motor | Homermobile |   1991 |
    |  2 | g349821031957 | Gratton | Robert  | 2121 des Jonquilles, Longueuil     | C8952457553 | F42680          | Chevrolet    | Camaro      |   1968 |
    +----+---------------+---------+---------+------------------------------------+-------------+-----------------+--------------+-------------+--------+

Ça semble fonctionner. Jusqu'à ce que Bob Gratton décide d'acheter une deuxième voiture. Pour inclure les informations d'immatriculation concernant ce véhicule, il faut ajouter un nouvel enregistrement :

    +----+---------------+---------+---------+------------------------------------+-------------+-----------------+--------------+-------------+--------+
    | id | no_permis     | nom     | prénom  | adresse                            | no_série    | immatriculation | marque       | modèle      | année  |
    +----+---------------+---------+---------+------------------------------------+-------------+-----------------+--------------+-------------+--------+
    |  1 | s213502031978 | Simpson | Homer   | 742 Evergreen Terrace, Springfield | H00000001   | 123 ABC         | Powell Motor | Homermobile |   1991 |
    |  2 | g349821031957 | Gratton | Robert  | 2121 des Jonquilles, Longueuil     | C8952457553 | F42680          | Chevrolet    | Camaro      |   1968 |
    |  3 | g349821031957 | Gratton | Robert  | 2121 des Jonquilles, Longueuil     | GKR7498779  | GKY 664         |    Ford      | Pinto       |   1971 | 
    +----+---------------+---------+---------+------------------------------------+-------------+-----------------+--------------+-------------+--------+
	
Vous remarquez un problème?

Non seulement certaines données sont inutilement dupliquées 

    +----+---------------+---------+---------+------------------------------------+-------------+-----------------+--------------+-------------+--------+
    | id | no_permis     | nom     | prénom  | adresse                            | no_série    | immatriculation | marque       | modèle      | année  |
    +----+---------------+---------+---------+------------------------------------+-------------+-----------------+--------------+-------------+--------+
    |  1 | s213502031978 | Simpson | Homer   | 742 Evergreen Terrace, Springfield | H00000001   | 123 ABC         | Powell Motor | Homermobile |   1991 |
    |  2 | g349821031957 | Gratton | Robert  | 2121 des Jonquilles, Longueuil     | C8952457553 | F42680          | Chevrolet    | Camaro      |   1968 |
    |  3 | g349821031957 | Gratton | Robert  | 2121 des Jonquilles, Longueuil     | GKR7498779  | GKY 664         |    Ford      | Pinto       |   1971 | 
    +----+---------------+---------+---------+------------------------------------+-------------+-----------------+--------------+-------------+--------+
    `----------------------------------------------------------------------------´
	                                 dupliqué
				 
mais il est aussi possible d'arriver à des incohérences dans les données. Par exemple, si Robert déménage et qu'on ne modifie par erreur son adresse que sur un seul des enregistrements :


    +----+---------------+---------+---------+------------------------------------+-------------+-----------------+--------------+-------------+--------+
    | id | no_permis     | nom     | prénom  | adresse                            | no_série    | immatriculation | marque       | modèle      | année  |
    +----+---------------+---------+---------+------------------------------------+-------------+-----------------+--------------+-------------+--------+
    |  1 | s213502031978 | Simpson | Homer   | 742 Evergreen Terrace, Springfield | H00000001   | 123 ABC         | Powell Motor | Homermobile |   1991 |
    |  2 | g349821031957 | Gratton | Robert  | 2121 des Jonquilles, Longueuil     | C8952457553 | F42680          | Chevrolet    | Camaro      |   1968 |
    |  3 | g349821031957 | Gratton | Robert  | 913 rue du Garage, St-Hubert       | GKR7498779  | GKY 664         |    Ford      | Pinto       |   1971 | 
    +----+---------------+---------+---------+------------------------------------+-------------+-----------------+--------------+-------------+--------+
                                               `---------------------------------´
											          données incohérentes
													  
Quelle est la source du problème ici? 

Est-ce trop demander qu'un propriétaire puisse posséder deux véhicules? Bien sûr que non. L'erreur a été de mélanger les données décrivant deux entités liées mais différentes (propriétaire et véhicule). Une table ne devrait représenter qu'une seule entité. 

Un truc pour savoir si on a affaire à une seule entité ou à deux entités distinctes A et B, est de se poser la question : «peut-il exister plus de A que de B?» ou «peut-il exister moins de A que de B?»

Peut-il exister plus de Véhicules que de Propriétaires de véhicule? Oui, bien entendu, dès qu'un propriétaire possède plus d'un véhicule.

Ici, les entités sont donc le véhicule et le propriétaire. De ce fait, on devra avoir deux tables distinctes, Véhicule et Propriétaire.
