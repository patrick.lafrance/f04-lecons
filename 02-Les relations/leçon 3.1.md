Il existe plusieurs méthodes pour représenter une relation entre deux entités. L'une des plus simples et des plus communes est dite en «patte d'oie» (nous verrons pourquoi bientôt).

Pour indiquer qu'il existe une relation entre deux entités, nous dessinerons une ligne d'une à l'autre. L'endroit exact où sont connectées les lignes sur les rectangles des entités est sans importance :

![schéma 1](https://www.dropbox.com/s/eghnhpaw9cnwvm6/saaq_1.jpeg?dl=1)

Cependant, toutes les relations ne sont pas identiques. Une des caractéristiques de la relation est le nombre d'éléments d'une entité qui interviennent dans la relation à l'autre entité. Considérons toujours la relation entre les entités Véhicule et Propriétaire. Dans notre exemple (quelque que soit la situation dans la réalité), combien de voitures peut posséder un propriétaire au maximum ? Autant qu'il veut. Il suffit, à chaque fois que le propriétaire achète une nouvelle voiture d'ajouter un enregistrement dans la table véhicule et de placer l'id du propriétaire dans le champ *propriétaire_id*. Un collectionneur pourrait sans problème immatriculer 10, 20, 1000 voitures s'il le désire.

À l'inverse, combien de propriétaires possèdent un véhicule donné ? Par exemple, la Camaro de Bob :

    +-------------+-----------------+--------------+-------------+--------+-----------------+
    | no_série    | immatriculation | marque       | modèle      | année  | propriétaire_id |
    +-------------+-----------------+--------------+-------------+--------+-----------------+
    | C8952457553 | F42680          | Chevrolet    | Camaro      |   1968 |               2 |
    +-------------+-----------------+--------------+-------------+--------+-----------------+
	
On sait qu'elle appartient à Bob grace à son id	(2) qui se trouve dans le champ propriétaire_id. Peut-on imaginer qu'elle soit partagée par plusieurs propriétaires (dans notre exemple, toujours)? Si c'était le cas, où mettre l'id du deuxième propriétaire? Sans modifier profondément le schéma, il est impossible d'avoir deux propriétaires pour le même véhicule.

Dans ce cas, on dira que la relation est *un à plusieurs*, c'est-à-dire qu'au maximum, *un* propriétaire possède *plusieurs* véhicules.

Le nombre de chaque entité impliqué dans la relation, c'est ce qu'on appelle la *cardinalité* de la relation et on la représente par des symboles à chaque bout de la ligne dessinée entre les entités. Une relation *un à plusieurs* est représentée ainsi :

![saaq 2](https://www.dropbox.com/s/vbokvtfoihak6mt/saaq_2.jpeg?dl=1)

Le symbole du côté de l'entité Véhicule s'appelle une «patte d'oie». Il faut imaginer qu'il s'agit du chemin qu'emprunte la clé étrangère. Elle part de l'entité Propriétaire et est copiée ou exportée vers l'entité Véhicule en plusieurs copies.

    Véhicule                                              Propriétaire
    +-------------+-----+-----------------+               +----+---------------+---+------------------------------------+
    | no_série    | ... | propriétaire_id |               | id | no_permis     |...| adresse                            |
    +-------------+-----+-----------------+               +----+---------------+---+------------------------------------+
    | C8952457553 | ... |               2 |←\             |  1 | s213502031978 |...| 742 Evergreen Terrace, Springfield |
    | H00000001   | ... |               1 |  -------------+  2 | g349821031957 |...| 2121 des Jonquilles, Longueuil     |
    | GKR7498779  | ... |               2 |←/             +----+---------------+---+------------------------------------+
    +-------------+-----+-----------------+

Attention! Les points de départ et d'arrivée de la ligne n'ont aucune signification. Par exemple, la même relation peut tout aussi bien être représentée de cette façon :

![saaq 2 vertical](https://www.dropbox.com/s/icn6w7nswiq1n98/saaq_2_vert.jpeg?dl=1)

À l'autre bout de la relation, on indiquera aussi le nombre d'éléments intervenant dans la relation. Pour indiquer qu'au maximum un propriétaire possède le véhicule (donc, que pour chaque enregistrement de véhicule, on n'a qu'un id de propriétaire), on utilise une barre verticale (comme le chiffre 1) :

![saaq 3](https://www.dropbox.com/s/0s18u5ex7dvw0ix/saaq_3.jpeg?dl=1)

Nous avons donc 1 clé primaire de propriétaire exporté dans plusieurs enregistrements de véhicules :

    Véhicule                                              Propriétaire
    +-------------+-----+-----------------+               +----+---------------+---+------------------------------------+
    | no_série    | ... | propriétaire_id |               | id | no_permis     |...| adresse                            |
    +-------------+-----+-----------------+               +----+---------------+---+------------------------------------+
    | C8952457553 | ... |               2 |←\             |  1 | s213502031978 |...| 742 Evergreen Terrace, Springfield |
    | H00000001   | ... |               1 |  -----------1-+  2 | g349821031957 |...| 2121 des Jonquilles, Longueuil     |
    | GKR7498779  | ... |               2 |←/             +----+---------------+---+------------------------------------+
    +-------------+-----+-----------------+

Toutes les combinaisons de *un* et de *plusieurs* forment des cardinalités de relation possibles :

* un à un : <br>
![1 à 1](https://www.dropbox.com/s/aepjgqs45y8lr7c/1-1.jpeg?dl=1)
chaque enregistrement d'une entité est associé avec un seul élément d'une autre entité. Exemple : un utilisateur et son profil.
<br><br>
* un à plusieurs ou plusieurs à un : <br>
![1 à n](https://www.dropbox.com/s/rir8uolmum4gxlu/1-n.jpeg?dl=1)
<br>
![n à 1](https://www.dropbox.com/s/og6epx0j2h0yv92/n-1.jpeg?dl=1)
<br>
comme on vient de le voir, un enregistrement est associé à plusieurs de l'autre entité.
<br><br>
* plusieurs à plusieurs : <br>
![n à n](https://www.dropbox.com/s/srw0h2hr5li2mhk/n-n.jpeg?dl=1)
<br>
plusieurs entités sont associés à plusieurs autres. Ce serait le cas dans notre exemple si un véhicule pouvait appartenir à plusieurs personnes en même temps (des co-propriétaires). Un autre exemple est celui des étudiants et de leurs cours. Un étudiant suit plusieurs cours et chaque cours est suivi par plusieurs étudiants. Nous verrons bientôt comment cette relation est réalisée.
