    DESC véhicule;
    +-----------------+--------------+------+-----+---------+-------+
    | Field           | Type         | Null | Key | Default | Extra |
    +-----------------+--------------+------+-----+---------+-------+
    | no_série        | varchar(255) | NO   | PRI | NULL    |       | <- clé primaire
    | immatriculation | varchar(7)   | NO   |     | NULL    |       |
    | marque          | varchar(255) | NO   |     | NULL    |       |
    | modèle          | varchar(255) | NO   |     | NULL    |       |
    | année           | int(11)      | NO   |     | NULL    |       |
    +-----------------+--------------+------+-----+---------+-------+


Pour le moment, avec les données que nous avons déjà entrées, nous pouvons facilement consulter la liste des propriétaires de véhicules :

    SELECT * FROM propriétaire;
    +----+---------------+---------+---------+------------------------------------+
    | id | no_permis     | nom     | prénom  | adresse                            |
    +----+---------------+---------+---------+------------------------------------+
    |  1 | s213502031978 | Simpson | Homer   | 742 Evergreen Terrace, Springfield |
    |  2 | g349821031957 | Gratton | Robert  | 2121 des Jonquilles, Longueuil     |
    +----+---------------+---------+---------+------------------------------------+

et la liste des véhicules immatriculés :

    SELECT * FROM véhicule;
    +-------------+-----------------+--------------+-------------+--------+
    | no_série    | immatriculation | marque       | modèle      | année  |
    +-------------+-----------------+--------------+-------------+--------+
    | C8952457553 | F42680          | Chevrolet    | Camaro      |   1968 |
    | H00000001   | 123 ABC         | Powell Motor | Homermobile |   1991 |
    | GKR7498779  | GKY 664         | Ford         | Pinto       |   1971 | 
    +-------------+-----------------+--------------+-------------+--------+

Mais comment savoir quel véhicule appartient à qui? Il faudrait avoir pour chaque véhicule une indication de son propriétaire : 

    SELECT * FROM véhicule;
    +-------------+-----------------+--------------+-------------+--------+-------------------------------+
    | no_série    | immatriculation | marque       | modèle      | année  | à qui appartient ce véhicule? |
    +-------------+-----------------+--------------+-------------+--------+-------------------------------+
    | C8952457553 | F42680          | Chevrolet    | Camaro      |   1968 |              ?                |
    | H00000001   | 123 ABC         | Powell Motor | Homermobile |   1991 |              ?                |
    | GKR7498779  | GKY 664         | Ford         | Pinto       |   1971 |              ?                | 
    +-------------+-----------------+--------------+-------------+--------+-------------------------------+

Quelle pourrait être cette indication? Il faudrait quelque chose d'unique, qui nous permette d'identifier le propriétaire de façon univoque. Il faudrait aussi que cette indication existe pour TOUS les propriétaires (sinon, comment l'identifier?) et idéalement, il faudrait qu'elle soit immuable dans le temps, ainsi, on ne craindrait pas qu'une de ces indication soit rendue invalide parce que son propriétaire a décidé de la changer.

Qu'avons-nous qui réunit ces trois caractéristiques : unique, non nullable et immuable?

🤔

Vraiment, je ne vois pas...


...


peut-être une ...


...


une clé?


...


...



une clé pri-


...


...


une clé primaire???!!!


