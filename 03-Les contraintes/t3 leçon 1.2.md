Souvenons-nous des clés étrangères et des relations.

![saaq 7](https://www.dropbox.com/s/xpjbm8uc2vun0mi/saaq_7.jpeg?dl=1)

Se lit "un et un seul propriétaire POSSÈDE 0, 1 ou plusieurs véhicules".

Regardons le contenu des tables de la relation:

```
SELECT * FROM propriétaire;
+----+---------------+---------+---------+------------------------------------+
| id | no_permis     | nom     | prénom  | adresse                            |
+----+---------------+---------+---------+------------------------------------+
|  1 | s213502031978 | Simpson | Homer   | 742 Evergreen Terrace, Springfield |
|  2 | g349821031957 | Gratton | Robert  | 2121 des Jonquilles, Longueuil     |
+----+---------------+---------+---------+------------------------------------+

SELECT * FROM véhicule;
+-------------+-----------------+--------------+-------------+--------+-----------------+
| no_série    | immatriculation | marque       | modèle      | année  | propriétaire_id |
+-------------+-----------------+--------------+-------------+--------+-----------------+
| C8952457553 | F42680          | Chevrolet    | Camaro      |   1968 |               2 |
| H00000001   | 123 ABC         | Powell Motor | Homermobile |   1991 |               1 |
| GKR7498779  | GKY 664         | Ford         | Pinto       |   1971 |               2 |
+-------------+-----------------+--------------+-------------+--------+-----------------+
```

Pour indiquer que le véhicule H00000001 (la Homermobile) est la propriété d'Homer Simpson,
nous avons ajouté son id dans la colonne propriétaire. Cet id correspond à celui de la 
table propriétaire.


Imaginez maintenant que nous insérons un nouveau véhicule: le Cybertruck de Tesla (une beauté!):

```
INSERT INTO véhicule VALUES ("CT12345", "TES 001", "Tesla", "Cybertruck", 2019, 5);
Query OK, 1 row affected (0.018 sec)

SELECT * FROM véhicule;
+-------------+-----------------+--------------+-------------+--------+------------------+
| no_série    | immatriculation | marque       | modèle      | année  | propriétaire_id  |
+-------------+-----------------+--------------+-------------+--------+------------------+
| C8952457553 | F42680          | Chevrolet    | Camaro      |   1968 |                2 |
| CT12345     | TES 001         | Tesla        | Cybertruck  |   2019 |                5 | <---- Oups!
| GKR7498779  | GKY 664         | Ford         | Pinto       |   1971 |                2 |
| H00000001   | 123 ABC         | Powell Motor | Homermobile |   1991 |                1 |
+-------------+-----------------+--------------+-------------+--------+------------------+
```

Remarquez que le propriétaire de ce véhicule n'existe pas (id = 5).
Le SGBD l'a tout de même accepté!

Détruisons le propriétaire Robert Gratton:

```
DELETE FROM propriétaire WHERE id = 2;
Query OK, 1 row affected (0.019 sec)

SELECT * FROM propriétaire;
+----+---------------+---------+---------+------------------------------------+
| id | no_permis     | nom     | prénom  | adresse                            |
+----+---------------+---------+---------+------------------------------------+
|  1 | s213502031978 | Simpson | Homer   | 742 Evergreen Terrace, Springfield |
+----+---------------+---------+---------+------------------------------------+

SELECT * FROM véhicule;
+-------------+-----------------+--------------+-------------+--------+------------------+
| no_série    | immatriculation | marque       | modèle      | année  | propriétaire_id  |
+-------------+-----------------+--------------+-------------+--------+------------------+
| C8952457553 | F42680          | Chevrolet    | Camaro      |   1968 |                2 |
| CT12345     | TES 001         | Tesla        | Cybertruck  |   2019 |                5 |
| GKR7498779  | GKY 664         | Ford         | Pinto       |   1971 |                2 |
| H00000001   | 123 ABC         | Powell Motor | Homermobile |   1991 |                1 |
+-------------+-----------------+--------------+-------------+--------+------------------+
```

Remarquez que les véhicules de Robert Gratton ont toujours le même id de propriétaire (2). 
Imaginez qu'on insère un nouveau propriétaire id=2, il hériterait automatiquement des 
véhicules de Robert Gratton... 
Il faudrait penser à retirer les id de clés étrangères "à la main". 

Pas très pratique et sujet à l'erreur...

