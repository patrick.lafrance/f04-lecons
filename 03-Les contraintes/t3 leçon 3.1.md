Nous voici à la 3e contrainte: la contrainte de validité.


Nous savons qu’un INT peut contenir des valeurs entre -2147483648 et 2147483647.

Avec la contrainte de validité nous pouvons limiter la valeur de cet attribut à un intervalle précis.

Le cas le plus typique est de limiter l’attribut à des valeurs positives ( >= 0 ) tel que le salaire ou le prix d’un produit ou service.
Il est aussi possible de valider le format d’une chaîne de caractères (Code postal, no de DA)
On peut vouloir valider qu’une date est inférieure à une autre.

Comme les deux autres contraintes, la validation se fait lors d’une insertion ou d’une modification d’un enregistrement.

Considérez la table suivante:

```
CREATE TABLE produit(
 id				INT 			PRIMARY KEY 	AUTO_INCREMENT,
 nom 			VARCHAR(255) 	NOT NULL,
 prix_coûtant 	INT 			NOT NULL,
 prix_vente	    INT
);
```

On veut s’assurer que l’attribut prix_coûtant sera toujours positif.
On ajoute une contrainte sur cet attribut :


```
CREATE TABLE produit(
 id				INT 			PRIMARY KEY 	AUTO_INCREMENT,
 nom 			VARCHAR(255) 	NOT NULL,
 prix_coûtant 	INT 			NOT NULL CHECK (prix_coûtant > 0),
 prix_vente	    INT
);
                                         ^^^^^^^^^^^^^^^^^^^^^^^^^
```

Cette façon de faire est la plus simple pour ajouter une contrainte sur un seul attribut.

Si on veut nommer la contrainte, on procède de cette façon :

```
CREATE TABLE produit(
 id				INT 			PRIMARY KEY 	AUTO_INCREMENT,
 nom 			VARCHAR(255) 	NOT NULL,
 prix_coûtant 	INT 			NOT NULL		CONSTRAINT coutant_positif_chk CHECK (prix_coûtant > 0),
 prix_vente	    INT
);
                                                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
```

La contrainte est nommée "coutant_positif_chk" dans le cas ci dessus.

Nous pouvons aussi déplacer la contrainte hors du champ:

```
CREATE TABLE produit(
 id				INT 			PRIMARY KEY 	AUTO_INCREMENT,
 nom 			VARCHAR(255) 	NOT NULL,
 prix_coûtant 	INT 			NOT NULL,
 prix_vente	    INT,
 CHECK (prix_coûtant > 0)
);
```

Ou encore, en la nommant :

```
CREATE TABLE produit(
 id				INT 			PRIMARY KEY 	AUTO_INCREMENT,
 nom 			VARCHAR(255) 	NOT NULL,
 prix_coûtant 	INT 			NOT NULL,
 prix_vente	    INT,
 CONSTRAINT coutant_positif_chk CHECK (prix_coûtant > 0)
);
```

La syntaxe simplifiée d’une contrainte de validité est très semblable aux autres: 

```
[CONSTRAINT [nom_de_contrainte]] CHECK (expression)
```

Où _expression_ est une expression booléenne (qui s'évalue à vrai ou faux) qui ne concerne 
que l'attribut visé par la contrainte.
