clé étrangère
relations 1-n 1-1 0,1-n etc

montrer un schéma, demander quelle est la clé étrangère
montrer des DESC et demander la déclaration de la clé étrangère

reprendre la question d'association entre le dessin de cardinalité et son 'nom' ?

Montrer une relation, demander dans quelle table va la clé étrangère.

L'Effet du not null dans une cardinalité.


Q1. Dans le schéma suivant, quelle est la clé étrangère?

--- img: quiz q1.jpg

*A) réalisateur_id
B) id
Feedback B) id est une clé primaire
C) titre
D) nom

Q2. Dans le schéma suivant, comment lit-on la relation ?

--- img: quiz q1.jpg

*A) un et un seul réalisateur réalise zéro, un ou plusieurs films
B) un et un seul film est réalisé par zéro, un ou plusieurs réalisateurs
C) un réalisateur réalise un film
D) plusieurs films sont réalisés par un réalisateur

Q3. Sachant que le champs réalisateur_id est au départ obligatoire,
qu'arrive-t-il à la relation si on le change pour facultatif?

*A) 0 ou 1 réalisateur réalise 0, 1 ou plusieurs films.
B) Rien ne change.
Feedback B) une realtion ayant une clé étrangère facultative permet l'absence de cette clé, donc minimalement zéro.
C) 0 ou 1 réalisateur réalise 0 ou 1 film. 
Feedback C) Le nombre de films réalisé par un réalisateur ne change pas.

Q4. 

Soient les tables événement et lieu.

```
DESC événement;
+---------+--------------+------+-----+---------+----------------+
| Field   | Type         | Null | Key | Default | Extra          |
+---------+--------------+------+-----+---------+----------------+
| id      | int(11)      | NO   | PRI | NULL    | auto_increment |
| titre   | varchar(255) | NO   |     | NULL    |                |
| artiste | varchar(255) | YES  |     | NULL    |                |
| date    | date         | YES  |     | NULL    |                |
| heure   | time         | YES  |     | NULL    |                |
+---------+--------------+------+-----+---------+----------------+

DESC lieu;
+-----------------+--------------+------+-----+---------+----------------+
| Field           | Type         | Null | Key | Default | Extra          |
+-----------------+--------------+------+-----+---------+----------------+
| id              | int(11)      | NO   | PRI | NULL    | auto_increment |
| nom             | varchar(255) | NO   |     | NULL    |                |
| adresse         | varchar(255) | YES  |     | NULL    |                |
| ville           | varchar(255) | NO   |     | NULL    |                |
| code_postal     | char(6)      | YES  |     | NULL    |                |
| no_téléphone    | char(10)     | YES  |     | NULL    |                |
| capacité        | int(11)      | NO   |     | NULL    |                |
| heure_fermeture | time         | YES  |     | NULL    |                |
+-----------------+--------------+------+-----+---------+----------------+
```

Sachant qu'un événement ne peut avoir lieu que dans un seul lieu sans nécessairement en avoir un d'attribué,
donnez la déclaration complète de la clé étrangère de lieu exportée dans la table événement.

*A) lieu_id INT
B) lieu_id INT NOT_NULL
Feedback B) puisqu'on peut ne pas attribuer de lieu à un événement, lieu_id peut être NULL
C) id INT
Feedback C) Il s'agit ici d'un attribut déjà utilisé pour la clé primaire artificielle
D) lieu_id VARCHAR(255)
Feedback D) une clé étrangère doit avoir le même type que la clé primaire à laquelle elle réfère
