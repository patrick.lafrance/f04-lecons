Considérez la table citoyen suivante:

```
CREATE TABLE citoyen(
 id					INT	PRIMARY KEY AUTO_INCREMENT
,nom				VARCHAR(255)	NOT NULL
,prénom				VARCHAR(255)	NOT NULL
,sexe				CHAR(1)	
,nas				CHAR(9)			NOT NULL
,date_naissance		DATE			NOT NULL
,date_décès			DATE
,CONSTRAINT ch_dates CHECK (date_naissance < date_décès)
,CONSTRAINT ch_nas   CHECK (CHAR_LENGTH(nas) = 9)
,CONSTRAINT ch_sexe  CHECK (UPPER(sexe) = "M" OR UPPER(sexe) = "F")
);
```

Serait-il possible d'ajouter une contrainte sur date_décès pour s'assurer que la date entrée
n'est pas située après la date d'aujourd'hui?

A) Non car date_décès peut être NULL.
Feedback A) NULL n'empêche pas les comparaisons
B) Oui: CHECK (date_décès < SYSDATE())
Feedback B) SYSDATE() est une fonction non déterministe.
*C) Non car la date courante s'obtient avec une fonction non déterministe




