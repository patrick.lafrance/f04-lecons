Voyons une autre contrainte.

Nous l'avons en quelque sorte déjà vue car c'est une caractéristique de la clé primaire.

Rappelons nous que les clés primaires sont OBLIGATOIRES (non nullable) et UNIQUES.

Nous savons déjà comment rendre un attribut obligatoire, nous verrons donc comment les rendre uniques.

Mais outre les clés primaires, quels attributs voudrait-on rendre unique?

Considérez la table étudiant suivante:

```
CREATE TABLE étudiant(
 no_da		CHAR(7)			PRIMARY KEY,
 nom		VARCHAR(255)	NOT NULL,
 prénom		VARCHAR(255)	NOT NULL,
 courriel	VARCHAR(255)	NOT NULL
);
```

L'identifiant unique de l'étudiant est son numéro de demande d'admission.
Un courriel doit aussi être unique!

La façon la plus simple de rendre un attribut unique est de l'ajouter dans sa ligne:

```
CREATE TABLE étudiant(
 no_da		CHAR(7)			PRIMARY KEY,
 nom		VARCHAR(255)	NOT NULL,
 prénom		VARCHAR(255)	NOT NULL,
 courriel	VARCHAR(255)	NOT NULL		UNIQUE
);
                                            ^^^^^^
```

Ainsi, il sera impossible d'avoir plusieurs étudiants ayant le même courriel.


On peut écrire aussi, de façon plus formelle, la contrainte d'unicité:

```
\[CONSTRAINT nom_contrainte\] UNIQUE (liste des attributs)
```

Dans la table étudiant:

```
CREATE TABLE étudiant(
 no_da		CHAR(7)			PRIMARY KEY,
 nom		VARCHAR(255)	NOT NULL,
 prénom		VARCHAR(255)	NOT NULL,
 courriel	VARCHAR(255)	NOT NULL,
 CONSTRAINT courriel_unq UNIQUE (courriel)
);
```

Ou sans la nommer:
```
CREATE TABLE étudiant(
 no_da		CHAR(7)			PRIMARY KEY,
 nom		VARCHAR(255)	NOT NULL,
 prénom		VARCHAR(255)	NOT NULL,
 courriel	VARCHAR(255)	NOT NULL,
 UNIQUE (courriel)
);
```

Encore une fois, c'est le SGBD qui choisi le nom s'il n'y en a pas.


On peut combiner plusieurs attributs dans la même contrainte. Pour ce faire il faut employer 
la syntaxe précédente, celle où la déclaration est listée et non à même la ligne de l'attribut.

C'est une distinction primordiale car si on écrit la création de la table client_combiné suivante:


```
CREATE TABLE client_combiné(
 id 		INT				PRIMARY KEY AUTO_INCREMENT,
 nom		VARCHAR(255)	NOT NULL,
 adresse	VARCHAR(255)	NOT NULL,
 CONSTRAINT nom_adresse_client_unq UNIQUE (nom, adresse)
);
```

Cela signifie que la _combinaison_ de *nom* et *prénom* doit être unique.

Tandis que la création de la table client_séparé suivante:

```
CREATE TABLE client_séparé(
 id 		INT				PRIMARY KEY AUTO_INCREMENT,
 nom		VARCHAR(255)	NOT NULL		UNIQUE,
 adresse	VARCHAR(255)	NOT NULL		UNIQUE
);
```

Signifie qu'aucun *nom* ne peut se répéter et qu'aucun *prénom* non plus.
Dans ce cas ci, il y a *deux* contraintes distinctes.

Par exemple, voyez le contenu de la table client_combiné:

```
SELECT * FROM client_combiné;
+----+--------+-------------------+
| id | nom    | adresse           |
+----+--------+-------------------+
|  3 | Marion | 123 Notre-Dame    |
|  1 | Robert | 123 Notre-Dame    |
|  2 | Robert | 34 rue Principale |
+----+--------+-------------------+
```

Notez qu'il y a deux "Robert" et deux personnes vivant au "123 Notre-Dame", ce serait impossible dans la table client_séparé.
Il serait impossible d'ajouter un 2e Robert au "123 Notre-Dame" dans la table client_combiné.


Sachez que NULL n'est pas considéré lors des comparaisons. 
C'est à dire que NULL n'est pas égal à NULL, donc il est possible d'avoir plusieurs
enregistrements avec NULL même s'il y a contrainte d'unicité.
(N'oubliez pas que NULL est l'état "absence de donnée" et non une donnée comme telle
et que la contrainte d'unicité assure que les _données_ sont uniques).


Par exemple, si la table client était plutôt ainsi:

```
CREATE TABLE client(
 id 		INT				PRIMARY KEY AUTO_INCREMENT,
 nom		VARCHAR(255)	NOT NULL,
 adresse	VARCHAR(255),                                <--- Facultatif!
 CONSTRAINT nom_adresse_client_unq UNIQUE (nom, adresse)
);

```

On pourrait avoir des données comme ceci:

```
SELECT * FROM client;
+----+--------+----------------+
| id | nom    | adresse        |
+----+--------+----------------+
|  3 | Marion | 123 Notre-Dame |
|  1 | Robert | NULL           |
|  2 | Robert | NULL           |
+----+--------+----------------+
```

Une contrainte d'unicité peut donc se résumer par: 
"toutes les données ou combinaisons de données complètement définies doivent 
être différentes des autres données ou combinaisons de données"
