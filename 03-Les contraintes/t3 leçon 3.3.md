
Tout comme la contrainte d'unicité, NULL n'est pas considéré lors de l'évaluation
de la contrainte de validité.

Si un attribut est NULL, le résultat sera considéré comme inconnu. 
Dans ce cas, la contrainte est considérée valide.

Exemple: 

```
CONSTRAINT profitable CHECK (prix_vente > prix_coûtant)
```

Si le prix de vente est NULL, l’expression devient :

```
NULL > prix_coûtant
```

Le résultat est inconnu et le SGBD va le considérer comme valide.

```
NULL > prix_coûtant → VRAI!
```

Insérons dans la table produit:

```
INSERT INTO produit(nom, prix_coûtant) VALUES ("Beigne expérimental", 100);
```

Ce beigne n'a pas de prix de vente qui est donc NULL.

Appliquons la contrainte:

```
CHECK (prix_vente > prix_coûtant)
CHECK (NULL       > 100         )
CHECK ( VRAI )
```

Donc le beigne expérimental respecte la contrainte et est ajouté:

```
SELECT * FROM produit;
+----+----------------------+---------------+------------+
| id | nom                  | prix_coûtant  | prix_vente |
+----+----------------------+---------------+------------+
|  1 | beigne nature        |            25 |        100 |
|  2 | Beigne expérimental  |           100 |       NULL |
+----+----------------------+---------------+------------+
```

## Autres particularités

La vérification de la contrainte se fait avec les données de l’enregistrement courant seulement.
Il est impossible d’utiliser les attributs de d’autres entités.

Dans la contrainte, il est possible d’utiliser les fonctions intégrées déterministes\* telles que CHAR_LENGTH, ABS, AVG, …
La contrainte de validité sont disponibles dans MariaDB depuis la version 10.2.
(Utilisateurs de Ubuntu: La version de MariaDB est 10.1 pour Ubuntu 18.04, il faut installer
manuellement une version plus récente).


\*Une fonction est dite déterministe si elle donne le même résultat à chaque utilisation. NOW() qui retourne la date courante n’aura jamais le même résultat d’un appel à l’autre et n’est donc pas déterministe.
