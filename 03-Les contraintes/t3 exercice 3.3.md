Considérez la table citoyen suivante:

```
CREATE TABLE citoyen(
 id					INT	PRIMARY KEY AUTO_INCREMENT
,nom				VARCHAR(255)	NOT NULL
,prénom				VARCHAR(255)	NOT NULL
,sexe				CHAR(1)	
,nas				CHAR(9)			NOT NULL
,date_naissance		DATE			NOT NULL
,date_décès			DATE
,CONSTRAINT ch_dates_chk CHECK (date_naissance < date_décès)
,CONSTRAINT ch_nas_chk   CHECK (CHAR_LENGTH(nas) = 9)
,CONSTRAINT ch_sexe_chk  CHECK (UPPER(sexe) = "M" OR UPPER(sexe) = "F")
);
```

Lequel de ces enregistrements sera accepté?

INSERT INTO citoyen(nom, prénom, sexe, nas, date_naissance, date_décès)
 
A) VALUES ("Gustav", "Carl", "M", "12345789", "1946-02-17", "2020-01-01");
Feedback A) Le NAS n'est pas de la bonne longueur
*B) VALUES ("Robin", "Stella", "f", "987654321", "1989-08-20", "1993-03-30");
C) VALUES ("Patenaude", "Brad", "M", "456123789", "1998-07-22", "1998-05-31");
Feedback C) La date de décès est avant la date de naissance.

