Que fait la contrainte _mystère_ dans la table suivante?:

```
CREATE TABLE étudiant(
 no_da		CHAR(7)			PRIMARY KEY
,nom		VARCHAR(255)	NOT NULL
,prénom		VARCHAR(255)	NOT NULL
,courriel	VARCHAR(255)	NOT NULL
,CONSTRAINT mystère CHECK (CHAR_LENGTH(no_da) > 0)
);
```

*A) Le no_da ne peut pas être une chaîne vide.
B) Le no_da doit être positif.
C) Le no_da ne peut pas être NULL.