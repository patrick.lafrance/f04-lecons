
Le troisième thème traite des contraintes.

Mais qu'est-ce qu'une contrainte?

  Dans le contexte des bases de données, une contrainte est une limite imposée à
  un ou plusieurs champs. Cette limite peut prendre plusieurs formes.

Par exemple, nous avons déjà vu deux façons de contraindre les données:

- Par le type (INT, CHAR(n), DATE, ...), qui détermine la nature d'un attribut
- Nullable ou non nullable, qui détermine si un attribut est obligatoire ou non.

Ces contraintes aident déjà à maintenir une certaine cohérence dans notre base de données.
En rendant les noms et prénoms obligatoires de la table client par exemple, on s'assure
qu'aucun client ne soit pas nommé, ce qui ne serait pas souhaitable. Disons qu'une compagnie
désire lancer une campagne de marketing personalisée, devrait-elle omettre de cibler les clients sans nom?

Nous utilisons donc les contraintes pour que le SGBD nous aide à garder une logique dans nos données.

Au cours de cette leçon, nous verrons trois nouvelles façons de contraindre nos données.

- La contrainte d'intégrité, qui concerne les clés étrangères
- La contrainte d'unicité, qui assure l'unicité d'une donnée
- La contrainte de validité, qui assure qu'une donnée respecte une expression booléenne  
de notre choix

À chaque insertion ```INSERT``` ou mise à jour ```UPDATE``` d'un enregistrement, la  
nouvelle donnée est confrontée à ces contraintes. Si elle ne les respecte pas, l'ajout 
ou la modification sera rejeté.


