Tout comme la contrainte d'unicité, il est possible de créer une contrainte
de validité qui concerne 2 ou plusieurs attributs.

Dans la table produit, nous pourrions vouloir que le prix coûtant soit
toujours inférieur au prix de vente.

```
CREATE TABLE produit(
 id				INT 			PRIMARY KEY 	AUTO_INCREMENT,
 nom 			VARCHAR(255) 	NOT NULL,
 prix_coûtant 	INT 			NOT NULL,
 prix_vente		INT,
 CHECK (prix_vente > prix_coûtant)
);
```

Comme pour la contrainte simple, il est possible de nommer la contrainte multiple :

```
CREATE TABLE produit(
 id				INT 			PRIMARY KEY 	AUTO_INCREMENT,
 nom 			VARCHAR(255) 	NOT NULL,
 prix_coûtant 	INT 			NOT NULL,
 prix_vente		INT,
 CONSTRAINT ch_profitable CHECK (prix_vente > prix_coûtant)
^^^^^^^^^^^^^^^^^^^^^^^^^
);
```

Voyons quelques exemples.

Supposons que la table produit ne contient qu'un seul enregistrement:

```
SELECT * FROM produit;
+----+---------------+---------------+------------+
| id | nom           | prix_coûtant  | prix_vente |
+----+---------------+---------------+------------+
|  1 | beigne nature |            25 |        100 |
+----+---------------+---------------+------------+
```

Ajoutons le produit "Beigne au chocolat"

```
INSERT INTO produit(nom, prix_coûtant, prix_vente) VALUES ("Beigne au chocolat", 50, 200);
Query OK, 1 row affected (0.030 sec)
```

Puisque le prix coûtant (50) est inférieur au prix de vente (200), l'enregistrement est accepté.
La table contient désormais deux produits:

```
SELECT * FROM produit;
+----+--------------------+---------------+------------+
| id | nom                | prix_coûtant  | prix_vente |
+----+--------------------+---------------+------------+
|  1 | beigne nature      |            25 |        100 |
|  2 | Beigne au chocolat |            50 |        200 |
+----+--------------------+---------------+------------+
```

Tentons maintenant de modifier le prix de vente du beigne nature:

```
UPDATE produit SET prix_vente=15 WHERE id=1;
```

L'enregistrement est rejeté car la nouvelle valeur de prix coûtant ne respecte pas
la contrainte.

```
UPDATE produit SET prix_vente=15 WHERE id=1;
ERROR 4025 (23000): CONSTRAINT `profitable` failed for `pâtisserie`.`produit`
```

En vérifiant à nouveau le contenu de la table, nous voyons que la modification n'a pas été faite:

```
SELECT * FROM produit;
+----+--------------------+---------------+------------+
| id | nom                | prix_coûtant  | prix_vente |
+----+--------------------+---------------+------------+
|  1 | beigne nature      |            25 |        100 |
|  2 | Beigne au chocolat |            50 |        200 |
+----+--------------------+---------------+------------+
```

