Considérez la table réalisateur:

CREATE TABLE réalisateur
(
 id					INT				PRIMARY KEY		AUTO_INCREMENT
,nom				VARCHAR(255)	NOT NULL
,prénom				VARCHAR(255)	NOT NULL
,date_naissance		DATE
,date_décès			DATE
);

Comment la table _film_ doit-elle être créée pour permettre l'insertion des données suivantes:

```
SELECT * FROM film;
+----+-------+-----------------+-----------------+--------+
| id | titre | année_parution  | réalisateur_id  | durée  |
+----+-------+-----------------+-----------------+--------+
|  1 | Rush  |            2013 |               1 |    123 |
|  2 | Rush  |            1991 |               2 |    120 |
|  3 | Virus |            1996 |               3 |     87 |
|  4 | Virus |            1995 |               4 |     92 |
|  5 | Virus |            1999 |               5 |     99 |
+----+-------+-----------------+-----------------+--------+
```


A)
``` 
CREATE TABLE film
(
 id					INT				PRIMARY KEY
,titre				VARCHAR(255)	NOT NULL	UNIQUE
,année_parution		SMALLINT
,réalisateur_id		INT				NOT NULL	UNIQUE
,durée				SMALLINT
);
```

Feedback A) Titre et réalisateur_id ont chacun leur contrainte d'unicité ici.
Plusieurs films du même titre ne seraient pas possibles dans ce cas.

*B)
```
CREATE TABLE film
(
 id					INT				PRIMARY KEY
,titre				VARCHAR(255)	NOT NULL
,année_parution		SMALLINT
,réalisateur_id		INT				NOT NULL
,durée				SMALLINT
,UNIQUE(titre, réalisateur_id)
);
```

C)
```
CREATE TABLE film
(
 id					INT				PRIMARY KEY
,titre				VARCHAR(255)	NOT NULL
,année_parution		SMALLINT
,réalisateur_id		INT				NOT NULL
,durée				SMALLINT
,UNIQUE(id, titre)
);
```
Feedback C) id est unique par lui même car il est une clé primaire. Ajouter un autre attribut à
la contrainte d'unicité n'apporte rien.