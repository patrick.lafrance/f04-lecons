Pour que le SGBD exige le respect des relations, il faut faire appel à une contrainte.

Une contrainte dite d'intégrité.

Voyons d'abord la syntaxe simplifiée de la contrainte t'intégrité:

```
[CONSTRAINT [nom_contrainte]] FOREIGN KEY (clé étrangère) 
REFERENCES table_référée (champs référés)
```

Ceci s'ajoute dans la création de la table:

```
CREATE TABLE véhicule(
 no_série			VARCHAR(255)	PRIMARY KEY,
 immatriculation	VARCHAR(7)		NOT NULL,
 marque				VARCHAR(255)	NOT NULL,
 modèle				VARCHAR(255)	NOT NULL,
 année				INT				NOT NULL,
 propriétaire_id	INT				NOT NULL,
 CONSTRAINT propriétaire_véhicule_fk FOREIGN KEY (propriétaire_id) REFERENCES propriétaire(id)
);
```

où:
- CONSTRAINT, FOREIGN KEY et REFERENCES sont des mots réservés
- propriétaire_véhicule_fk est le nom de la contrainte,
- propriétaire_id est l'attribut qui est la clé étrangère
- propriétaire(id) signifie que cela réfère à la clé primaire 'id' de la table 'propriétaire' 

On peut aussi écrire la contrainte sans la nommer:

```
CREATE TABLE véhicule(
 no_série			VARCHAR(255)	PRIMARY KEY,
 immatriculation	VARCHAR(7)		NOT NULL,
 marque				VARCHAR(255)	NOT NULL,
 modèle				VARCHAR(255)	NOT NULL,
 année				INT				NOT NULL,
 propriétaire_id	INT				NOT NULL,
 FOREIGN KEY (propriétaire_id) REFERENCES propriétaire(id)
);
```

Dans ce cas-ci, c'est le SGBD qui choisit le nom.

À chaque insertion ou modification d'un enregistrement dans une des tables de la
relation, le SGBD vérifie d'abord si le changment respecte cette contrainte.
C'est-à-dire que la valeur de la clé primaire est bel et bien présente dans la table référée.

Par exemple, si nous voulions ajouter un véhicule ayant un propriétaire qui n'existe pas:

```
INSERT INTO véhicule VALUES ("CT12345", "TES 001", "Tesla", "Cybertruck", 2019, 5);
ERROR 1452 (23000): Cannot add or update a child row: a foreign key constraint fails 
(`saaq`.`véhicule`, CONSTRAINT `véhicule_ibfk_1` FOREIGN KEY (`propriétaire_id`) REFERENCES `propriétaire` (`id`))
```

Puisque l'id 5 n'existe pas dans la table des propriétaires, indiquer que ce véhicule est
la propriété de quelqu'un qui n'existe pas est une erreur.
Donc le SGBD refuse la modification (d'où l'erreur affichée)

La relation entre les deux tables est vérifiée des deux côtés, pas uniquement si on change
les données de la table où la contrainte est écrite.

Autre exemple: si on détruisait le propriétaire "Robert Gratton":

```
DELETE FROM propriétaire WHERE id = 2;
ERROR 1451 (23000): Cannot delete or update a parent row: a foreign key constraint fails 
(`saaq`.`véhicule`, CONSTRAINT `véhicule_ibfk_1` FOREIGN KEY (`propriétaire_id`) REFERENCES `propriétaire` (`id`))
```

Le SGBD refuse l'effacement car la perte du propriétaire id=2 ferait en sorte que dans la table
véhicule le propriétaire id=2 n'existerait plus, ce qui serait une erreur.

