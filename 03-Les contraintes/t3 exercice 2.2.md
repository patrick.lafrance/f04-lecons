Considérez la table suivante:

CREATE TABLE événement(
 id              INT           PRIMARY KEY AUTO_INCREMENT
,titre           VARCHAR(255)  NOT NULL
,artiste		 VARCHAR(255)
,date            DATE
,heure           TIME
,lieu_id         INT           NOT NULL
,CONSTRAINT événement_lieu_fk FOREIGN KEY (lieu_id) REFERENCES lieu(id)
,UNIQUE(date, heure, lieu_id)
);

En imposant la contrainte UNIQUE(date, heure, lieu_id),
deux événements peuvent-ils avoir la même date et heure?

*A) Oui, à condition que le lieu ne soit pas défini.
B) Oui, sans condition.
Feedback B) Si ces deux événements ont le même lieu cela enfreindrait la contrainte d'unicité.
C) Non, la contrainte l'empêche.
Feedback C) N'oubliez pas que NULL n'est pas évalué dans les comparaisons.
D) Non, ce n'est jamais possible.
Feedback D) Il faut considérer ce que contient le 3e attribut qui compose la contrainte d'unicité.