Laquelle des contraintes de validité suivantes est correcte?

```
CREATE TABLE lieu(
 id              INT           PRIMARY KEY AUTO_INCREMENT
,nom             VARCHAR(255)  NOT NULL
,adresse         VARCHAR(255)
,ville           VARCHAR(255)  NOT NULL
,code_postal     CHAR(6)
,no_téléphone    CHAR(10)
,capacité        INT           NOT NULL
,heure_fermeture TIME
,CONSTRAINT const_A_chk CHECK (no_téléphone)
,CONSTRAINT const_B_chk CHECK (adresse >= 0)
,CONSTRAINT const_C_chk CHECK (CHAR_LENGTH(lieu) > 0)
,CONSTRAINT const_D_chk CHECK (capacité > 0)
);
```

Feedback A: n'est pas une expression booléene
Feedback B: il ne fait pas de sens de comparer une chaîne avec un entier
Feedback C: lieu est le nom de la table et non un attribut
Rép: const_D_chk