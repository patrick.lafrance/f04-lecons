Soit le schéma suivant:


```
CREATE TABLE propriétaire(
id INT PRIMARY KEY
,no_permis VARCHAR(255)
,nom 	VARCHAR(255) NOT NULL
,prénom	VARCHAR(255) NOT NULL
,adresse VARCHAR(255)
);

CREATE TABLE véhicule(
 no_série			VARCHAR(255)	PRIMARY KEY
,immatriculation	VARCHAR(7)		NOT NULL
,marque				VARCHAR(255)	NOT NULL
,modèle				VARCHAR(255)	NOT NULL
,année				INT				NOT NULL
,propriétaire_id	INT				NOT NULL
,FOREIGN KEY (propriétaire_id) REFERENCES propriétaire(id)
);
```

et les données suivantes:

```
SELECT * FROM propriétaire;
+----+---------------+---------+---------+------------------------------------+
| id | no_permis     | nom     | prénom  | adresse                            |
+----+---------------+---------+---------+------------------------------------+
|  1 | s213502031978 | Simpson | Homer   | 742 Evergreen Terrace, Springfield |
|  2 | g349821031957 | Gratton | Robert  | 2121 des Jonquilles, Longueuil     |
+----+---------------+---------+---------+------------------------------------+

SELECT * FROM véhicule;
+-------------+-----------------+--------------+-------------+--------+------------------+
| no_série    | immatriculation | marque       | modèle      | année  | propriétaire_id  |
+-------------+-----------------+--------------+-------------+--------+------------------+
| C8952457553 | F42680          | Chevrolet    | Camaro      |   1968 |                2 |
| GKR7498779  | GKY 664         | Ford         | Pinto       |   1971 |                2 |
| H00000001   | 123 ABC         | Powell Motor | Homermobile |   1991 |                1 |
+-------------+-----------------+--------------+-------------+--------+------------------+

```

comment faire pour effacer le propriétaire _Homer Simpson_ ?

a) DELETE FROM propriétaire WHERE id = 1;
Feedback A) La contrainte d'intégrité ne le permettera pas avant d'avoir défait le lien entre
            le véhicule et son propriétaire.
b) C'est impossible
Feedback B) C'est possible, il faut cependant le faire en plusieurs étapes.
*c) Il faut d'abord effacer le véhicule qui lui est attaché.
d) Il faut d'abord rendre le propriétaire de ses véhicules à NULL.
Feedback D) Ce serait une bonne solution si propriétaire_id était facultatif.
